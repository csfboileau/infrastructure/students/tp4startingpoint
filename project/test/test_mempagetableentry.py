import unittest

from main.memanalysis import pagetableentry as pte
from main.utils import conversion as conv


class PTEntryTests(unittest.TestCase):

	def test_contructor(self):
		ptEntry = pte.PTEntry( 16, False, 16, [44,8])

		self.assertEqual(False, ptEntry.isSwappedOut())
		self.assertEqual(16, ptEntry.getPageID())
		self.assertEqual(16, ptEntry.getPageLocation())
		self.assertEqual(False, ptEntry.processHasAccessToPage(40))
		self.assertEqual(True, ptEntry.processHasAccessToPage(44))
		self.assertEqual(True, ptEntry.processHasAccessToPage(0))
		self.assertEqual(True, ptEntry.processHasAccessToPage(8))
		ptEntry.giveProcessAccessToPage(40)
		self.assertEqual(True, ptEntry.processHasAccessToPage(40))
		self.assertRaises(ValueError, ptEntry.giveProcessAccessToPage, 0)
		self.assertRaises(ValueError, ptEntry.giveProcessAccessToPage, 44)

		expected = dict()
		expected['pageLocation'] = 16
		expected['isSwappedOut'] = False
		expected['ACL'] = [0,8,40,44]
		self.assertEqual(expected, ptEntry.toMap())

	def test_contructor2(self):
		ptEntry = pte.PTEntry( 16, False, 16, [44,8])
		self.assertRaises(ValueError, pte.PTEntry, "16", False, 16, [44,8])
		self.assertRaises(ValueError, pte.PTEntry, 16, True, 16, ["44-9",8])
		self.assertRaises(ValueError, pte.PTEntry, "55-4", True, "55-4", [44,8])
		self.assertRaises(ValueError, pte.PTEntry, 16, False, 16.56, [44,8])



	def test_AddPage(self):
		processes = []
		for i in range(1, 63):
			processes.append(i)
		ptEntry = pte.PTEntry(16, False, 16, processes)
		self.assertRaises(ValueError, ptEntry.giveProcessAccessToPage, 77)

# 	def test_parseFromHexDump(self):
# #		ptEntry = PTEntry.PTEntry( 16, False, 16, [44,8])
# 		hexDump = "00102C08"
# 		while len(hexDump) < pte.ENTRY_SIZE*2:
# 			hexDump = hexDump + "00"
# 		ptEntry = pte.parseFromHexDump(hexDump, 16)
# 		self.assertEqual(False, ptEntry.isSwappedOut())
# 		self.assertEqual(16, ptEntry.getPageID())
# 		self.assertEqual(16, ptEntry.getPageLocation())
# 		self.assertEqual(False, ptEntry.processHasAccessToPage(40))
# 		self.assertEqual(True, ptEntry.processHasAccessToPage(44))
# 		self.assertEqual(True, ptEntry.processHasAccessToPage(0))
# 		self.assertEqual(True, ptEntry.processHasAccessToPage(8))
# 		ptEntry.giveProcessAccessToPage(40)
# 		self.assertEqual(True, ptEntry.processHasAccessToPage(40))
# 		self.assertRaises(ValueError, ptEntry.giveProcessAccessToPage, 0)
# 		self.assertRaises(ValueError, ptEntry.giveProcessAccessToPage, 44)

	def test_parseFromHexDump2(self):
		hexDump = "00102C08" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000"
		self.assertRaises(ValueError, pte.parseFromHexDump, hexDump, 16)
		self.assertRaises(ValueError, pte.parseFromHexDump, "", 16)
		self.assertRaises(ValueError, pte.parseFromHexDump, "80", 16)
		while len(hexDump) < pte.ENTRY_SIZE*2:
			hexDump = hexDump + "00"
		hexDump = hexDump + "0"
		self.assertRaises(ValueError, pte.parseFromHexDump, hexDump, 16)

	# def test_parseFromHexDump3(self):
	# 	hexDump = "80102C08"
	# 	while len(hexDump) < pte.ENTRY_SIZE*2:
	# 		hexDump = hexDump + "00"
	# 	ptEntry = pte.parseFromHexDump(hexDump, 16)
	# 	self.assertEqual(True, ptEntry.isSwappedOut())

if __name__ == "__main__":
	unittest.main()
