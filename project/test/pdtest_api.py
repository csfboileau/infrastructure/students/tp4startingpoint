import unittest
import requests

IP = "tp4-infra.ddnsgeek.com" #provide the right server IP/name
PORT = "5555"
URL = "http://" + IP + ":" + PORT

class APITest(unittest.TestCase):

	def test_home(self):
		response = requests.get(URL + "/")
		self.assertEqual(200, response.status_code)

	def test_prod_mode(self):
		response = requests.get(URL + "/")
		self.assertEqual(200, response.status_code)
		self.assertTrue("---PROD MODE---" in response.content.decode('utf-8'))



if __name__ == "__main__":
	unittest.main()
