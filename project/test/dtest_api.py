import unittest
import requests
import json
from main import memoryapi

IP = "0.0.0.0"
PORT = "15555"
URL = "http://" + IP + ":" + PORT

class APITest(unittest.TestCase):

	def test_MetaRoute(self):
		response = requests.get(URL + "/metainfo/3")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		map = map['metaInfo']
		self.assertEqual("4D4C4D4C", map['magicNumber'])
		self.assertEqual("56534D4C", map['memoryLayoutType'])

		#self.assertEqual(16, map['pageSize'])
		#self.assertEqual(64, map['nbPhysicalPages'])
		#self.assertEqual(0, map['nbSwapPages'])
		#self.assertEqual(1, map['nbBitmapPages'])
		#self.assertEqual(2, map['nbPTPages'])
		#self.assertEqual(1, map['nbPLTPages'])

	# def test_BitmapRoute(self):
	# 	response = requests.get(URL + "/bitmap/3")
	# 	self.assertEqual(200, response.status_code)
	# 	map = json.loads(response.content.decode('utf-8'))
	# 	expected = [0,1,2,3,4,6, 11, 16, 21, 22, 32]
	# 	actual = map['bitmapInfo']
	# 	#self.assertCountEqual(expected, actual)

	def check(self, map, id, swappedOut, acl):
		self.assertEqual(swappedOut, map['isSwappedOut'])
		self.assertEqual(id, map['pageLocation'])
		self.assertEqual(acl, map['ACL'])

	# def test_ptRoute(self):
	# 	response = requests.get(URL + "/pagetable/3")
	# 	self.assertEqual(200, response.status_code)
	# 	map = json.loads(response.content.decode('utf-8'))
	# 	map = map['ptInfo']

		# self.assertEqual(11, len(map))

		# self.assertIn('0', map)
		# self.check(map['0'], 0, False, [0])

		# self.assertIn('1', map)
		# self.check(map['1'], 1, False, [0])

		# self.assertIn('2', map)
		# self.check(map['2'], 2, False, [0])

		# self.assertIn('3', map)
		# self.check(map['3'], 3, False, [0])

		# self.assertIn('4', map)
		# self.check(map['4'], 4, False, [0])

		# self.assertIn('6', map)
		# self.check(map['6'], 6, False, [0,30])

		# self.assertIn('11', map)
		# self.check(map['11'], 11, True, [0,30])

		# self.assertIn('16', map)
		# self.check(map['16'], 16, False, [0,8,44])

		# self.assertIn('21', map)
		# self.check(map['21'], 21, False, [0, 8])

		# self.assertIn('22', map)
		# self.check(map['22'], 22, False, [0,30])

		# self.assertIn('32', map)
		# self.check(map['32'], 32, False, [0, 8])


	# def test_pltRoute(self): 
	# 	response = requests.get(URL + "/processlookup/3") 
	# 	self.assertEqual(200, response.status_code) 
	# 	map = json.loads(response.content.decode('utf-8')) 
	# 	map = map['pltInfo']

		# self.assertEqual(4, len(map))

		# self.assertIn('0', map)
		# self.assertEqual([0, 1, 2, 3, 4], map['0'])

		# self.assertIn('8', map)
		# self.assertEqual([16, 32, 21], map['8'])

		# self.assertIn('30', map)
		# self.assertEqual([22, 6, 11], map['30'])

		# self.assertIn('44', map)
		# self.assertEqual([16], map['44'])

	# def test_getPhysicalAddressRouteFail1(self):
	# 	response = requests.get(URL + "/physicaladdress/3?pid=30") 
	# 	self.assertEqual(400, response.status_code) 

	def test_getPhysicalAddressRouteFail2(self):
		response = requests.get(URL + "/physicaladdress/3?vadd=4028") 
		self.assertEqual(400, response.status_code) 

	def test_getPhysicalAddressRouteFail3(self):
		response = requests.get(URL + "/physicaladdress/3") 
		self.assertEqual(400, response.status_code) 

	def test_getPhysicalAddressRouteFail6(self):
		response = requests.get(URL + "/physicaladdress/3?pid=33&vadd=28") 
		self.assertEqual(422, response.status_code) 

	def test_getPhysicalAddressRouteFail7(self):
		response = requests.get(URL + "/physicaladdress/3?pid=30&vadd=1828") 
		self.assertEqual(422, response.status_code) 

	def test_getPhysicalAddressRouteFail8(self):
		response = requests.get(URL + "/physicaladdress/3?pid=8&vadd=1801") 
		self.assertEqual(422, response.status_code) 

	# def test_getPhysicalAddressRoute1(self):
	# 	response = requests.get(URL + "/physicaladdress/3?pid=30&vadd=828") 
	# 	self.assertEqual(200, response.status_code) 
	# 	map = json.loads(response.content.decode('utf-8')) 
	# 	actual = map['physicalAddress']
	# 	self.assertEqual('3028', actual)

	# def test_getPhysicalAddressRoute2(self):
	# 	response = requests.get(URL + "/physicaladdress/3?pid=30&vadd=1028") 
	# 	self.assertEqual(200, response.status_code) 
	# 	map = json.loads(response.content.decode('utf-8')) 
	# 	actual = map['physicalAddress']
	# 	self.assertEqual('5828', actual)

	# def test_getPhysicalAddressRoute3(self):
	# 	response = requests.get(URL + "/physicaladdress/3?pid=8&vadd=801") 
	# 	self.assertEqual(200, response.status_code) 
	# 	map = json.loads(response.content.decode('utf-8')) 
	# 	actual = map['physicalAddress']
	# 	self.assertEqual('10001', actual)

	# def test_getPhysicalAddressRoute4(self):
	# 	response = requests.get(URL + "/physicaladdress/3?pid=8&vadd=1001") 
	# 	self.assertEqual(200, response.status_code) 
	# 	map = json.loads(response.content.decode('utf-8')) 
	# 	actual = map['physicalAddress']
	# 	self.assertEqual('A801', actual)

if __name__ == "__main__":
	unittest.main()
