import unittest

from main.memanalysis import processlookupentry as plte
from main.utils import conversion as conv


class PLTEntryTests(unittest.TestCase):
	EMPTY_PLTENTRY = "00000000" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000"

	def test_contructor(self):
		pltEntry = plte.PLTEntry(True, 16, [44,8])

		self.assertEqual(True, pltEntry.hasPage(44))
		self.assertEqual(True, pltEntry.hasPage(8))
		self.assertEqual(False, pltEntry.hasPage(45))
		self.assertEqual(False, pltEntry.hasPage(0))
		self.assertEqual(16, pltEntry.getPID())
		self.assertEqual([44, 8], pltEntry.getPages())

	def test_contructor2(self):
		pages = []
		for i in range(1, 33):
			pages.append(i)

		self.assertRaises(ValueError,plte.PLTEntry, True, 16, pages)


	def test_AddPage(self):
		pltEntry = plte.PLTEntry(True, 16, [44,8])
		pltEntry.addPage(13)
		pltEntry.addPage(58)
		pltEntry.addPage(21)

		self.assertEqual(True, pltEntry.hasPage(44))
		self.assertEqual(True, pltEntry.hasPage(8))
		self.assertEqual(True, pltEntry.hasPage(13))
		self.assertEqual(True, pltEntry.hasPage(58))
		self.assertEqual(True, pltEntry.hasPage(21))
		self.assertEqual(False, pltEntry.hasPage(45))
		self.assertEqual(False, pltEntry.hasPage(0))

	def test_AddPage(self):
		pages = []
		for i in range(1, 31):
			pages.append(i)
		pltEntry = plte.PLTEntry(True, 16, pages)
		pltEntry.addPage(58)
		self.assertRaises(ValueError, pltEntry.addPage, 37)
		self.assertEqual(True, pltEntry.hasPage(58))
		self.assertEqual(True, pltEntry.hasPage(21))
		self.assertEqual(False, pltEntry.hasPage(45))
		self.assertEqual(False, pltEntry.hasPage(0))

	def test_AddPage(self):
		pltEntry = plte.PLTEntry(False, 13, [])
		self.assertEqual(False, pltEntry.isUsed())
		self.assertRaises(ValueError, pltEntry.hasPage, 37)
		pltEntry.addPage(58)
		self.assertEqual(True, pltEntry.isUsed())

		for i in range(0, 30):
			pltEntry.addPage(i)

		self.assertRaises(ValueError, pltEntry.addPage, 33)

	def test_parseFromHexDump(self):
		hexDump = "842C0025" + "05000000" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000"
		pltEntry = plte.parseFromHexDump(hexDump, 16)
		self.assertEqual(True, pltEntry.isUsed())
		self.assertEqual(True, pltEntry.hasPage(44))
		self.assertEqual(True, pltEntry.hasPage(0))
		self.assertEqual(True, pltEntry.hasPage(37))
		self.assertEqual(True, pltEntry.hasPage(5))
		self.assertEqual(False, pltEntry.hasPage(21))
		self.assertEqual(False, pltEntry.hasPage(45))
		self.assertEqual(False, pltEntry.hasPage(1))
		self.assertEqual(4, pltEntry.nbPagesAllocated)

	def test_parseFromHexDump2(self):
		hexDump = "00000000" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000"
		pltEntry = plte.parseFromHexDump(hexDump, 13)
		self.assertEqual(False, pltEntry.isUsed())
		self.assertRaises(ValueError, pltEntry.hasPage, 37)
		pltEntry.addPage(37)
		self.assertEqual(True, pltEntry.hasPage(37))
		self.assertEqual(1, pltEntry.nbPagesAllocated)

	def test_parseFromHexDump2(self):
		hexDump = "00000001" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000"
		self.assertRaises(ValueError, plte.parseFromHexDump, hexDump, 13)

	def test_parseFromHexDump4(self):
		hexDump = "80000000" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000" + "00000000"
		pltEntry = plte.parseFromHexDump(hexDump, 16)
		self.assertEqual(True, pltEntry.isUsed())
		self.assertEqual(False, pltEntry.hasPage(44))
		self.assertEqual(False, pltEntry.hasPage(0))
		self.assertEqual(0, pltEntry.nbPagesAllocated)




if __name__ == "__main__":
	unittest.main()
