import unittest
import requests
import json
from main import memoryapi

IP = "localhost"
PORT = "15555"
URL = "http://" + IP + ":" + PORT

PAGES = [0,1,2,3,4,5,6,7,8,9,10, 28, 42, 84, 100, 101]


class APITest(unittest.TestCase):

	def test_MetaRoute(self):
		response = requests.get(URL + "/metainfo/5")
		self.assertEqual(200, response.status_code)

		map = json.loads(response.content.decode('utf-8'))
		map = map['metaInfo']
		self.assertEqual("4D4C4D4C", map['magicNumber'])
		self.assertEqual("56534D4C", map['memoryLayoutType'])

		# self.assertEqual(8, map['pageSize'])
		# self.assertEqual(128, map['nbPhysicalPages'])
		# self.assertEqual(0, map['nbSwapPages'])
		# self.assertEqual(1, map['nbBitmapPages'])
		# self.assertEqual(8, map['nbPTPages'])
		# self.assertEqual(1, map['nbPLTPages'])

	# def test_BitmapRoute(self):
	# 	response = requests.get(URL + "/bitmap/5")
	# 	self.assertEqual(200, response.status_code)
	# 	map = json.loads(response.content.decode('utf-8'))
	# 	actual = map['bitmapInfo']
	# 	self.assertCountEqual(PAGES, actual)

	def check(self, map, id, swappedOut, acl):
		self.assertEqual(swappedOut, map['isSwappedOut'])
#		self.assertEqual(id, map['pageID'])
		self.assertEqual(id, map['pageLocation'])
		self.assertEqual(acl, map['ACL'])

	# def test_ptRoute(self):
	# 	response = requests.get(URL + "/pagetable/5")
	# 	self.assertEqual(200, response.status_code)
	# 	map = json.loads(response.content.decode('utf-8'))
	# 	map = map['ptInfo']

	# 	self.assertEqual(16, len(map))

	# 	for i in PAGES:
	# 		self.assertIn(str(i), map)
	# 		if i < 11:
	# 			self.check(map[str(i)], i, False, [0])

	# 	self.check(map['28'], 28, False, [0,2])
	# 	self.check(map['84'], 84, False, [0,3])
	# 	self.check(map['100'], 100, False, [0,4])
	# 	self.check(map['101'], 101, False, [0,5])
	# 	self.check(map['42'], 42, False, [0,3])


	# def test_pltRoute(self): 
	# 	response = requests.get(URL + "/processlookup/5") 
	# 	self.assertEqual(200, response.status_code) 
	# 	map = json.loads(response.content.decode('utf-8')) 
	# 	map = map['pltInfo']

		# self.assertEqual(5, len(map))

		# self.assertIn('0', map)
		# self.assertEqual([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], map['0'])

		# self.assertIn('2', map)
		# self.assertEqual([28], map['2'])

		# self.assertIn('3', map)
		# self.assertEqual([84, 42], map['3'])

		# self.assertIn('4', map)
		# self.assertEqual([100], map['4'])

		# self.assertIn('5', map)
		# self.assertEqual([101], map['5'])


	# def test_getPhysicalAddressRouteFail1(self):
	# 	response = requests.get(URL + "/physicaladdress/5?pid=3&vadd=828")
	# 	self.assertEqual(422, response.status_code)

	# def test_getPhysicalAddressRoute1(self):
	# 	response = requests.get(URL + "/physicaladdress/5?pid=3&vadd=28")
	# 	self.assertEqual(200, response.status_code)
	# 	map = json.loads(response.content.decode('utf-8'))
	# 	actual = map['physicalAddress']
	# 	self.assertEqual('15028', actual)

	# def test_getPhysicalAddressRoute2(self):
	# 	response = requests.get(URL + "/physicaladdress/5?pid=3&vadd=428")
	# 	self.assertEqual(200, response.status_code)
	# 	map = json.loads(response.content.decode('utf-8'))
	# 	actual = map['physicalAddress']
	# 	self.assertEqual('A828', actual)

if __name__ == "__main__":
	unittest.main()
