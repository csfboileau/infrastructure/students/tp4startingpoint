import unittest
import requests
import json
from main import memoryapi

IP = "localhost"
PORT = "15555"
URL = "http://" + IP + ":" + PORT

class APITest(unittest.TestCase):

	def test_MetaRoute(self):
		response = requests.get(URL + "/metainfo/4")
		self.assertEqual(200, response.status_code)
		
		map = json.loads(response.content.decode('utf-8'))
		map = map['metaInfo']
		
		self.assertEqual("4D4C4D4C", map['magicNumber'])
		self.assertEqual("56534D4C", map['memoryLayoutType'])

		self.assertEqual(16, map['pageSize'])
		#self.assertEqual(96, map['nbPhysicalPages'])
		#self.assertEqual(0, map['nbSwapPages'])
		#self.assertEqual(1, map['nbBitmapPages'])
		#self.assertEqual(3, map['nbPTPages'])
		#self.assertEqual(1, map['nbPLTPages'])

	def test_BitmapRoute(self):
		response = requests.get(URL + "/bitmap/4")
		self.assertEqual(200, response.status_code)
		map = json.loads(response.content.decode('utf-8'))
		expected = [0,1,2,3,4,5, 12, 22, 32, 42, 52, 62, 95]
		actual = map['bitmapInfo']
		#self.assertCountEqual(expected, actual)

	def check(self, map, id, swappedOut, acl):
		self.assertEqual(swappedOut, map['isSwappedOut'])
#		self.assertEqual(id, map['pageID'])
		self.assertEqual(id, map['pageLocation'])
		self.assertEqual(acl, map['ACL'])

	# def test_ptRoute(self):
	# 	response = requests.get(URL + "/pagetable/4")
	# 	self.assertEqual(200, response.status_code)
	# 	map = json.loads(response.content.decode('utf-8'))
	# 	map = map['ptInfo']

		# self.assertEqual(13, len(map))

		# self.assertIn('0', map)
		# self.check(map['0'], 0, False, [0])

		# self.assertIn('1', map)
		# self.check(map['1'], 1, False, [0])

		# self.assertIn('2', map)
		# self.check(map['2'], 2, False, [0])

		# self.assertIn('3', map)
		# self.check(map['3'], 3, False, [0])

		# self.assertIn('4', map)
		# self.check(map['4'], 4, False, [0])

		# self.assertIn('5', map)
		# self.check(map['5'], 5, False, [0])

		# self.assertIn('12', map)
		# self.check(map['12'], 12, False, [0,2])

		# self.assertIn('22', map)
		# self.check(map['22'], 22, False, [0,2])

		# self.assertIn('32', map)
		# self.check(map['32'], 32, False, [0,2])

		# self.assertIn('42', map)
		# self.check(map['42'], 42, False, [0,2])

		# self.assertIn('52', map)
		# self.check(map['52'], 52, False, [0,2])

		# self.assertIn('62', map)
		# self.check(map['62'], 62, False, [0,2])

		# self.assertIn('95', map)
		# self.check(map['95'], 95, False, [0,3])


	def test_pltRoute(self): 
		response = requests.get(URL + "/processlookup/4") 
		self.assertEqual(200, response.status_code) 
		map = json.loads(response.content.decode('utf-8')) 
		map = map['pltInfo']

		# self.assertEqual(3, len(map))

		# self.assertIn('0', map)
		# self.assertEqual([0, 1, 2, 3, 4, 5], map['0'])

		# self.assertIn('2', map)
		# self.assertEqual([32, 12, 62, 42, 52, 22], map['2'])

		# self.assertIn('3', map)
		# self.assertEqual([95], map['3'])

if __name__ == "__main__":
	unittest.main()
