import unittest

from main.memanalysis import bitmap as bm
from main.utils import conversion as conv

class BitmapPageTests(unittest.TestCase):

	def test_getElementBoundaries(self):
		nbPages = 320
		bitmapPage = bm.BitmapPage(nbPages)
		with self.assertRaises(ValueError):
			bitmapPage.isPageUsed(-1)

	def test_getElementBoundaries2(self):
		nbPages = 320
		bitmapPage = bm.BitmapPage(nbPages)
		with self.assertRaises(ValueError):
			bitmapPage.isPageUsed(320)

	def test_getElement(self):
		nbPages = 320
		bitmapPage = bm.BitmapPage(nbPages)
		expected = False
		actual = bitmapPage.getPageStatus(319)
		self.assertEqual(expected, actual)

	def test_getElement(self):
		nbPages = 320
		bitmapPage = bm.BitmapPage(nbPages)
		expected = False
		actual = bitmapPage.isPageUsed(0)
		self.assertEqual(expected, actual)


	# def test_parseHex(self):
	# 	hexDump = "AB00EF"
	# 	bitmapPage = bm.parseFromHexDump(hexDump, 3)
	# 	self.assertEqual(True, bitmapPage.isPageUsed(0))
	# 	self.assertEqual(False, bitmapPage.isPageUsed(1))
	# 	self.assertEqual(True, bitmapPage.isPageUsed(2))
	# 	self.assertEqual(False, bitmapPage.isPageUsed(3))

	# 	self.assertEqual(False, bitmapPage.isPageUsed(19))
	# 	self.assertEqual(True, bitmapPage.isPageUsed(20))
	# 	self.assertEqual(True, bitmapPage.isPageUsed(21))
	# 	self.assertEqual(True, bitmapPage.isPageUsed(22))
		
	# 	expected = [0,2,4,6,7, 16,17,18,20,21,22, 23]
	# 	self.assertCountEqual(expected, bitmapPage.getAllUsedPages())


	def test_parseHex2(self):
		hexDump = "AB00EF"
		with self.assertRaises(ValueError):
			bm.parseFromHexDump(hexDump, 25)

	# def test_parseHex3(self):
	# 	hexDump = "AB00EF0000000000000000000000000000000000000000000000"
	# 	with self.assertRaises(ValueError):
	# 		bm.parseFromHexDump(hexDump, 25)


if __name__ == "__main__":
	unittest.main()

