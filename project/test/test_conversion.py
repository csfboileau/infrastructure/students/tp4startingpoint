import unittest

from main.utils import conversion

class ConversionTests(unittest.TestCase):

	def test_hexToInt(self):
		self.assertEqual(16, conversion.hexToInt('10'))
		self.assertEqual(15, conversion.hexToInt('F'))
		self.assertEqual(17, conversion.hexToInt('11'))
		self.assertEqual(32, conversion.hexToInt('20'))

	def test_extractSequence1(self):
		with self.assertRaises(ValueError):
			conversion.extractSequence("AABBCCDDEEFF", -3, 3)

	def test_extractSequence2(self):
		with self.assertRaises(ValueError):
			conversion.extractSequence("AABBCCDDEEFF", 0, 7)

	def test_extractSequence3(self):
		with self.assertRaises(ValueError):
			conversion.extractSequence("AABBCCDDEEFF", 4, 3)

	def test_extractSequence4(self):
		dataBlock = "AABBCCDDEEFF"
		expected = "AABBCCDDEEFF"
		actual = conversion.extractSequence(dataBlock, 0, 6)
		self.assertEqual(expected, actual)

	def test_extractSequence5(self):
		dataBlock = "AABBCCDDEEFF"
		expected = "BBCC"
		actual = conversion.extractSequence(dataBlock, 1, 2)
		self.assertEqual(expected, actual)

	def test_extractSequence6(self):
		dataBlock = "AABBCCDDEEFF"
		expected = ""
		actual = conversion.extractSequence(dataBlock, 3, 0)
		self.assertEqual(expected, actual)

	def test_extractSequence7(self):
		dataBlock = "AABBCCDDEEFF"
		expected = "EEFF"
		actual = conversion.extractSequence(dataBlock, 4, 2)
		self.assertEqual(expected, actual)

	def test_extractSequence8(self):
		dataBlock = "AABBCCDDEEFF"
		expected = "AABBCC"
		actual = conversion.extractSequence(dataBlock, 0, 3)
		self.assertEqual(expected, actual)

	def test_readBlockFromFileInHex(self):
		actual = conversion.readBlockFromFileInHex("./project/files/dump3.vsml", 0, 20)
		expected = "4D4C4D4C56534D4C001000400000000100020001" #len = 40
		self.assertEqual(expected, actual)


if __name__ == "__main__":
	unittest.main()

