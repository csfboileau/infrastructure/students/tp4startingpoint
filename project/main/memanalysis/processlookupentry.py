from main.utils import conversion as conv

#TODO In theory, the pagesAllocated collection cannot contains duplicate, but must maintain insertion order. Maybe switch to a better Datastructure later on.

SIZE = 128 #in bits

class PLTEntry:
	#SIMPLIFICATION: These constants are shortcuts. In theory, we should get this info from the spec of the memory layout.
	MAX_NBPAGES = 31

	def __init__(self, isUsed, processID, pagesAllocated):
		if len(pagesAllocated) > PLTEntry.MAX_NBPAGES:
			raise ValueError("Cannot create a PLT entry with (" + str(len(pagesAllocated))  + ") pages allocated. Maximum value is (" + str(PLTEntry.MAX_NBPAGES)  + ").")
		self.processID = processID
		self.nbPagesAllocated = len(pagesAllocated)
		self.pagesAllocated = pagesAllocated
		self.__isUsed = isUsed

	def getPID(self):
		return self.processID

	def getPages(self):
		return self.pagesAllocated.copy()

	def isUsed(self):
		return self.__isUsed

	def addPage(self, pageId):
		if self.nbPagesAllocated >= PLTEntry.MAX_NBPAGES:
			raise ValueError("The process (" + str(self.processID)  + ") currently has (" + str(self.nbPagesAllocated)  + ") allocated pages which is the max. Cannot allocate any more.")
		self.__isUsed = True
		if(not pageId in self.pagesAllocated):
			self.nbPagesAllocated = self.nbPagesAllocated + 1
			self.pagesAllocated.append(pageId)

	def hasPage(self, pageId):
		if not self.__isUsed:
			raise ValueError("The PLT entry for process (" + str(self.processID)  + ") is not used, so it cannot be queried.")
		return pageId in self.pagesAllocated

def parseFromHexDump(hexDump, pid):
	nbPagesAllocated = conv.hexToInt(hexDump[0:2])
	isUsed = False
	if nbPagesAllocated > 127: 
		isUsed = True
		nbPagesAllocated = nbPagesAllocated - 128
	if not isUsed:
		if hexDump.count("0") != len(hexDump):
			raise ValueError("The PLT entry to parse is unused but contains non 0 data.")
	allocatedPages = []
	for i in range(1, nbPagesAllocated+1):
		hexOfPage = hexDump[2*i:2*i+2]
		page = conv.hexToInt(hexOfPage)
		allocatedPages.append(page)
	return PLTEntry(isUsed, pid, allocatedPages)
