import os
from datetime import *

from main.memanalysis import meta
from main.utils import conversion as conv

#TODO remove for students
#from os import path
def getMode(): #pragma: no cover
	if "INFRA_TP4_MODE" in os.environ:
		return os.environ["INFRA_TP4_MODE"]
	return "N/A"

def isInProd(): #pragma: no cover
	if getMode() == "PROD":
		return True;
	elif getMode() == "TEST":
		return False;
	print("No mode was specified, we are assuming TEST mode.", flush=True)
	return False;


def getDumpFileName(fileNumber): #pragma: no cover
	return "./project/files/dump" + str(fileNumber) + ".vsml"

def getPage(fileNumber, pageNumber):
#TODO switch the following lines for students
	fileName = getDumpFileName(fileNumber)

	#First, we load the first part of the MetaPage to get the pageSize
	pageSizeOctets = 128 #a first guess, should be enough to get at least up to the pageSize part of the Metapage
	dump = conv.readBlockFromFileInHex(fileName, 0, pageSizeOctets)
	pageSizeOctets = meta.peekPageSizeOctets(dump)

	#now we know the true pageSize of the memory dump, we can now get the page we want.
	offset = pageSizeOctets*pageNumber

	dump = conv.readBlockFromFileInHex(fileName, offset, pageSizeOctets)
	return dump
