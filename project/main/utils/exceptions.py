class MemoryDumpFormatException(Exception):
	pass

class InactivePidException(Exception):
	pass

class VirtualAddressOutsidePhysicalMemoryException(Exception):
	pass
